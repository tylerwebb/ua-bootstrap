<div id="catstrap-sidebar" data-spy="affix" data-offset-top="303" class="hidden-print hidden-xs hidden-sm affix-top">
    <ul class="nav bs-docs-sidenav">
        <li>
            [How to use](#how-to-use)
            <ul class="nav">
                <li>[Instruction](#instructions)</li>
                <li>[Fallback Fonts](#fallback-fonts)</li>
                <li>[Examples in CSS](#examples-in-css)</li>
            </ul>
        </li>
        <li>
            [Milo Serif Web](#milo-serif-web)
            <ul class="nav">
                <li>[Serif Normal](#serif-normal)</li>
                <li>[Serif Medium](#serif-medium)</li>
            </ul>
        </li>
        <li>
            [Milo Serif Black](#milo-serif-black)
            <ul class="nav">
                <li>[Serif Normal Black](#serif-normal-black)</li>
                <li>[Serif Normal Black Italic](#serif-normal-black-italic)</li>
            </ul>
        </li>
        <li>
            [Milo Web](#milo-web)
            <ul class="nav">
                <li>[Normal](#normal)</li>
                <li>[Medium](#medium)</li>
                <li>[Italics](#italics)</li>
                <li>[Bold](#bold)</li>
            </ul>
        </li>
    </ul>
</div>
